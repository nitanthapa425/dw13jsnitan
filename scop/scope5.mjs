let a = 3;
let b = 9;
{
  //child

  {
    //grand child
    a = 5;
  }
  console.log(a); //5
}
console.log(a); //5
console.log(b); //9

/* 

parent
a=5
b=9


child


grand child


*/
