//"nitan"="Nitan"

let firstLetterCapital = (name) => {
  // "nitan" => ["n","i","t","a","n"] => ["N","i","t","a","n"] ="Nitan"

  let arrayName = name.split(""); //["n","i","t","a","n"]

  let outputArray = arrayName.map((value, i) => {
    if (i === 0) {
      return value.toUpperCase();
    } else {
      return value.toLowerCase();
    }
  });

  let output = outputArray.join("");
  return output;
};

// let _firstLetterCapital = firstLetterCapital("nitan"); //Nitan
// let b = firstLetterCapital("hari"); //Hari
// console.log(__firstLetterCapital);

// "my name is nitan" => "My Name Is Nitan"

//                  split(" ")                           map                            join(" ")
// "my name is nitan" = ["my", "name", "is", "nitan"] =>["My", "Name", "Is","Nitan"] => "My Name Is Nitan"

let eachWordCapital = (input) => {
  let inputArray = input.split(" "); //["my", "name", "is", "nitan"]

  let outputArray = inputArray.map((value, i) => {
    return firstLetterCapital(value);
  });

  let output = outputArray.join(" "); //"My Name Is Nitan"

  return output;
};

let _eachWordCapital = eachWordCapital("hello nitan");

console.log(_eachWordCapital);
