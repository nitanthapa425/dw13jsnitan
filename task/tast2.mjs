let product = [
  {
    name: "laptop",
    price: 100000,
  },
  {
    name: "mobile",
    price: 50000,
  },
  {
    name: "tv",
    price: 70000,
  },
];

//["laptop","tv"]   => product whose price is greater than 60000

/* 

[
  {
    name: "laptop",
    price: 100000,
  },
  {
    name: "tv",
    price: 70000,
  },
];
 */

let output1 = product
  .filter((value, i) => {
    if (value.price > 60000) {
      return true;
    }
  })
  .map((value, i) => {
    return value.name;
  });

console.log(output1);

let output = product.filter((value, i) => {
  if (value.price > 60000) {
    return true;
  }
});
console.log(output);

// ["laptop","mobile","tv"]
// [100000, 5000]
// [{name:"laptop",price:100000},{name:"tv",price:70000}]=> product whose price is greater than 60000
let arrayOfName = product.map((value, i) => {
  return value.name;
});

// console.log(arrayOfName);

let arrayOfPrice = product.map((value, i) => {
  return value.price;
});

// console.log(arrayOfPrice);
