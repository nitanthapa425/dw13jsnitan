//&& (and operator) output will be true if all are true
console.log(true && true && false && true); //false
console.log(true && true && true && true); //true
//|| (or operator)  output will be true if one of them is true

console.log(true || true || false || false); //true
console.log(false || false || false || false); //false

//! not operator

console.log(!true); //false
console.log(!false); //true
console.log(!(false || false || false || false)); //true
