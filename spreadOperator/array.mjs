let ar1 = ["a", "b", "c"];
let ar2 = [1, 2, 3];

//["a","b","c",1,2,3]
//[1,2,3,"a","b","c"]
// spread operator are the wrapper opener

let ar = [3, 4, ...ar1, 10, ...ar2]; // [3,4,"a","b","c",10,1,2,3]
console.log(ar);
