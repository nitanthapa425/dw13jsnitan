let info = {
  name: "nitan",
  age: 30,
  isMarried: false,
};

let info1 = { ...info }; //...{name:"nitan",age:30,isMarried:false}  =  {name:"nitan",age:30,isMarried:false}

let info2 = { ...info, address: "gagalphedi" };
console.log(info2);
