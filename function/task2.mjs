/* 
make a functin called sum 
pass 2, value
the fun must return the sum of passed value
*/

let sum = (num1, num2) => {
  //   let num3 = num1 + num2;
  //   return num3;

  return num1 + num2; //it is better approach
};

let _sum = sum(1, 2);
console.log(_sum);

let average = (num1, num2, num3) => {
  return (num1 + num2 + num3) / 3;
};

let _average = average(1, 2, 3);
console.log(_average);
