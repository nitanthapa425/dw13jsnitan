let a = NaN;
console.log(a === NaN);

// we can not check NaN from this way

//to check whether a variable is NaN or not we muse use isNaN() function
console.log(isNaN(a));
