let ar1 = ["nitan", 29, false];

let v = ar1.includes("ram");
console.log(v); //false

let v1 = ar1.includes("tan");
console.log(v1); //false

let v3 = ar1.includes(29);
console.log(v3); //true

// make a arrow function
// pass array of  fruits
// the function must return "the fruits contain apple" if the array contain "apple"
//  else return "the fruits does not contain apple"

let hasApple = (fruits) => {
  let _hasApple = fruits.includes("apple"); //true

  if (_hasApple) {
    return "the fruits contain apple";
  } else {
    return "the fruits does not contain apple";
  }
};

let __hasApple = hasApple(["mango", "banana", "apple"]);
console.log(__hasApple);
