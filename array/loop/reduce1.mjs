let ar1 = [1, 2, 4]; // product of all element 1*2*4=8

let allProduct = ar1.reduce((pre, cur) => {
  return pre * cur;
}, 1);
console.log(allProduct);
