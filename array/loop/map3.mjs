// ["my","name","is"] => ["MY","NAME","IS"]

let input = ["my", "name", "is"];

//output = [3,6,9]
let output = input.map((value, i) => {
  return value.toUpperCase();
});
console.log(output);
