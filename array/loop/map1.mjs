// [1,2,3] => [3,6,9]

let input = [1, 2, 3];

//output = [3,6,9]
let output = input.map((value, i) => {
  return value * 3;
});
console.log(output);
