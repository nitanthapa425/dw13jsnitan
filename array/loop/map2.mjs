// [1,2,3]=>[11,12,13]
let input = [11, 12, 13];

//output = [3,6,9]
let output = input.map((value, i) => {
  return value * 10;
});
console.log(output);
