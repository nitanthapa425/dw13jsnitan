//[1,3,4,5] it must return [100,300,0,500]
// here odd number is multiplied by 100 and even number is multiplied by 0

let input = [1, 3, 4, 5];

// [100,300,0,500]
let output = input.map((value, index) => {
  if (value % 2 === 0) {
    return value * 0;
  } else {
    return value * 100;
  }
});
console.log(output);
