let ar1 = ["nitan", 29, false];
//          0,      1 ,   2

console.log(ar1.length);

// make a arrow function
// pass array of names
// the fun must return "arrays length is greater than 3"  if passes array length is greater than 3
// else the fun must return "array length is less than 3"

let isGreaterThan3 = (names) => {
  if (names.length > 3) {
    return "arrays length is greater than 3";
  } else {
    return "array length is less than 3";
  }
};

let _isGreaterThan3 = isGreaterThan3(["nitan", "ram"]);
console.log(_isGreaterThan3);
