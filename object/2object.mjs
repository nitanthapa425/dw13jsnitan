let info = {
  name: "nitan",
  favFruits: ["apple", "grapes"],
  location: {
    country: "nepal",
    province: "bagmati",
    city: "kathmandu",
  },
  age: () => {
    console.log("i am 30");
  },
};

// console.log(info.name)
// console.log(info.favFruits);
// console.log(info.favFruits[1]);
// console.log(info.location);
// console.log(info.location.country);
info.age();
