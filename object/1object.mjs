// let info = ["nitan", 30, false]

// array is a collection of value
// where as object is a collection of key value pairs
// key value pair are called property

let info = {
  name: "nitan",
  age: 30,
  isMarried: false,
};

// get value
console.log(info);
console.log(info.name);
console.log(info.age);
console.log(info.isMarried);
//change value
info.age = 31;
info.isMarried = true;
console.log(info);

//delete isMarried field
delete info.isMarried;
console.log(info);
