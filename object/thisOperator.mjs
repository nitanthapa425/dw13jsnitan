let info = {
  name: "nitan",
  surname: "thapa",
  fullName: () => {
    console.log(this.name);
    console.log(this.surname);
  },
};

/*
what is this operator => this is a operator which point itself
difference between arrow function and normal function
syntax, this operator does not support in arrow function

*/

console.log(info.name); //nitan
info.fullName();
