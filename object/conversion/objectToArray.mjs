let info = {
  name: "nitan",
  age: 30,
  isMarried: false,
};

//object => array
/* 
["name","age","isMarried"] keys
["nitan",30, false] values
[["name","nitan"], ["age",30],["isMarried",false]]
 */

// let keysArray = Object.keys(info);//["name","age", "isMarried"]
// console.log(keysArray);

// let valuesArray = Object.values(info); //[ 'nitan', 30, false ]
// console.log(valuesArray);

let propertyArray = Object.entries(info);
console.log(propertyArray);
