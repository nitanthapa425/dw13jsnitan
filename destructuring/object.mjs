// let info = {
//   name: "nitan",
//   age: 30,
//   isMarried: false,
// };
// console.log(info.name);
// console.log(info.age);
// console.log(info.isMarried);

let { age, isMarried, name } = { name: "nitan", age: 30, isMarried: false };
// in object destructuring. order does not matter
//name must match
console.log(name);
console.log(age);
console.log(isMarried);
